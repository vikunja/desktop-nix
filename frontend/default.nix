{ stdenv
, lib
, mkYarnPackage
, nodejs
, yarn
, unstableGitUpdater
, fetchFromGitHub
}:

let
  version = "unstable";

  src = fetchFromGitHub {
    githubBase = "kolaente.dev"; # Using this until we can use fetchFromGitea
    owner = "vikunja";
    repo = "frontend";
    rev = "f9e5a17907321b34091934daa33dc210f611f561";
    sha256 = "0hs6b3zljbiwjxx4fwabz2a8cmj7pshvmfz7lwx4xzhddav306vy";
  };

  frontend-modules = mkYarnPackage rec {
    name = "vikunja-frontend-modules";
    inherit version src;
    doDist = false;
  };
in stdenv.mkDerivation {
  pname = "vikunja-frontend";
  inherit version src;

  nativeBuildInputs = [ frontend-modules yarn ];

  buildPhase = ''
    # Cannot use symlink or postcss-loader will crap out
    cp -r ${frontend-modules}/libexec/vikunja-frontend/node_modules/ .
    yarn --offline run build
    sed -i 's/\/api\/v1//g' dist/index.html
  '';

  installPhase = ''
    cp -r dist $out
  '';

  meta = {
    description = "Frontend of the Vikunja to-do list app";
    homepage = "https://vikunja.io/";
    license = lib.licenses.agpl3Plus;
    maintainers = with lib.maintainers; [ kolaente ];
    platforms = lib.platforms.all;
  };

  passthru.updateScript = unstableGitUpdater {
    url = "${src.meta.homepage}.git";
  };
}

