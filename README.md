# Vikunja Desktop for NixOS

This repo contains a collection `.nix` files to build the [Vikunja frontend](https://code.vikunja.io/frontend) and the [electron-based desktop app](https://code.vikunja.io/desktop) for NixOS systems.

## Building locally

```
nix-build -E 'with import <nixpkgs> {}; callPackage ./default.nix {}'
```

